import {
  Body,
  Button,
  Header,
  Icon,
  Right,
  Title,
  Item,
  Input
} from "native-base";
import React from "react";
import StatusBarManger from "./StatusBarManager";

const HeaderComp = props => {
  return (
    <Header>
      <StatusBarManger />

      <Body>
        <Title>{props.title}</Title>
      </Body>

      <Right>
        <Button transparent>
          <Icon name="cart" style={{ color: "black" }} />
        </Button>
      </Right>
    </Header>
  );
};

export default HeaderComp;
